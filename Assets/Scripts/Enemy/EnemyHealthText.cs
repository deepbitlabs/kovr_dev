﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthText : MonoBehaviour
{
    public GameObject Enemy;

    void Update()
    {
        GetComponent<TextMesh>().text = "Health" + (Enemy.GetComponent<EnemyController>().curHealth.ToString());
    }
}
