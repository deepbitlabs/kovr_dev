﻿using UnityEngine;
using System.Collections;

public class HitColider : MonoBehaviour
{
    ///public PlayerController player;
    ///
    public PlayerHealth playerHealth;

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has been hit! ");
				playerHealth.curHealth -= 5f;
				//mBlur.enabled = true;

			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has not been hit! ");
				return;
			}
		}
	}
}
