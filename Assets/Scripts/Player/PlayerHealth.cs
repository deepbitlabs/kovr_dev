﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public float maxHealth = 100f;
    public float curHealth;

    public GameController game;

    private float damageBloodAmount = 3;
    private float maxBloodIndication = 0.5f;
    float recoverSpeed = 1;

    // Player damage visuals

    bool isDead;

    void Awake()
    {
        curHealth = maxHealth;
    }

    void Update()
    {
        // Writing functionality that will check if the player health is at a low rate
        // Inorder to do other effects

        curHealth += recoverSpeed * Time.deltaTime;

        if (curHealth <= 0)
        {
            if (!isDead)
            {
                curHealth = 0;
                Debug.Log("Player is knocked out!");

                killPlayer();
            }
        }

        BleedBehavior.minBloodAmount = maxBloodIndication * (maxHealth - curHealth) / maxHealth;
    }

    public void Damage(int amount)
    {
        BleedBehavior.BloodAmount += Mathf.Clamp01(damageBloodAmount * amount / curHealth);

        curHealth -= amount;
    }

    public void killPlayer()
    {
        // Player is knockedout... Initialize UI elements!!!
        game.DisplayGameOver();
    }


}
