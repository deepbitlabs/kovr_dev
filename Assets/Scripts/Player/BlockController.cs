﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    public GameObject ShieldObject;

    public bool isBlocking;

    // Player Health
    public PlayerHealth playerHealth;

    private void Start()
    {
        //Shield
        playerHealth = GetComponent<PlayerHealth>();
    }

    private void OnDestroy()
    {
       
    }

    private void Update()
    {
        if (ShieldObject == null)
            return;

        if (isBlocking)
            Block();
    }

    public void Block()
    {

    }

    //private void SetShield(Shield shield)
    //{
    //    if (shield.isLocal)
    //    {
    //        ShieldObject = shield.gameObject;
    //    }
    //    else
    //    {
    //        if (!shield.isLocal)
    //        {
    //            shieldObject = shield.gameObject;
    //        }
    //    }
    //}
}
