﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

public class PlayerController : MonoBehaviour
{
    // Player Health
    public PlayerHealth playerHealth;

    // Player is dead
    bool isDead;


    // GameManager
	public GameController gameController;
	private EnemyController enemy;


	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has been hit! ");
				playerHealth.curHealth -= 3f;
				//GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>().enabled = true;
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "RHitCollider" || col.gameObject.tag == "LHitCollider")
		{
			if (gameObject.tag == "Player") 
			{
				Debug.Log(" Player has not been hit! ");
				//GetComponent<UnityStandardAssets.ImageEffects.MotionBlur>().enabled = false;
				return;
			}
		}
	}
}
