﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class PunchableBodyParts : MonoBehaviour 
{
	public EnemyController enemy;

    public EnemyAI enemyAI;

	public bool isTrigger;

	public AudioClip punch;

	private AudioSource punchSound;

    public ScoreManager score;

	void Awake()
	{
		punchSound = gameObject.AddComponent<AudioSource> ();
		punchSound.clip = punch;
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Left" || col.gameObject.tag == "Right") 
		{
			if (gameObject.tag == "Head") 
			{
				enemy.curHealth -= 2f;
				Debug.Log (gameObject.tag + " Collider works! " + enemy.curHealth + " has decreased by ");
				punchSound.Play ();

                score.UpdateScores();
			}

			if (gameObject.tag == "Spine") 
			{
				enemy.curHealth -= 1f;
				Debug.Log (gameObject.tag + " Collider works! " + enemy.curHealth + " has decreased by ");
				punchSound.Play ();

                score.UpdateScores();
            }

			if (gameObject.tag == "Hips") 
			{
				enemy.curHealth -= 0.5f;
				Debug.Log (gameObject.tag + " Collider works! " + enemy.curHealth + " has decreased by ");
				punchSound.Play ();

                score.UpdateScores();
            }
		}
	}
}
