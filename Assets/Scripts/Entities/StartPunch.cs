﻿using UnityEngine;
using System.Collections;

public class StartPunch : MonoBehaviour 
{

    public GameController gameController;

    public EnemyController enemy;
    public EnemyAI enemyAI;


    void Start()
    {
        //enemy.enabled = false;

        enemy.GetComponent<EnemyController>().enabled = false;
        enemyAI.GetComponent<EnemyAI>().enabled = false;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Left" || col.gameObject.tag == "Right")
        {
            if (gameObject.tag == "StartFight")
            {
                gameController.startFight.SetActive(false);
                enemy.GetComponent<EnemyController>().enabled = true;
                enemyAI.GetComponent<EnemyAI>().enabled = true;

            }
        }
    }
}
