﻿using UnityEngine;
using System.Collections;
using Valve.VR;
using System;
using System.IO;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class GloveController : MonoBehaviour 
{
    SteamVR_TrackedObject trackedObj;
    SteamVR_Controller.Device device;

    private GloveSoundManager gloveSoundManager;
    private bool isMenuDisplayed = false;

    private Vector3 rightGlovePositionOffset = new Vector3(-0.0883f, 0, -.122f);
    private Vector3 leftGlovePositionOffset = new Vector3(.1f, 0, -.09f);
    private bool hasGloveBeenSet = false;
    private GameObject gloveModel;
	private GameController gameController;


    string[] forDebugging = new string[3];
    private bool vectorsRecorded = false;


    void Start()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        device = SteamVR_Controller.Input((int)trackedObj.index);
        gloveSoundManager = GetComponent<GloveSoundManager>(); 
    } 

    void FixedUpdate()
    {
        device = SteamVR_Controller.Input((int)trackedObj.index);

        if (!hasGloveBeenSet)
        {
            if(this.gameObject.tag == "Right")
            {
                //gloveModel = GetComponentInChildren<MeshRenderer>().gameObject;
                //gloveModel.transform.Rotate(0, 90f, 180f);
                //Debug.Log(gloveModel.transform.rotation.eulerAngles);
                //gloveModel.transform.localPosition = rightGlovePositionOffset;
                hasGloveBeenSet = true;

            }

            if (this.gameObject.tag == "Left")
            {
                //gloveModel = GetComponentInChildren<MeshRenderer>().gameObject;
                //gloveModel.transform.Rotate(0, 270f, 0f);
                //Debug.Log(gloveModel.transform.rotation.eulerAngles);
                //gloveModel.transform.localPosition = leftGlovePositionOffset;
                hasGloveBeenSet = true;

            }
            else
                Debug.Log("Objects tag does not match requirement to fix glove  position");
        }

        if (device.GetPressUp(EVRButtonId.k_EButton_ApplicationMenu) && !isMenuDisplayed)
        {
            //Invoke("DisplayMenu", 0.1f);
			SceneManager.LoadScene("MainMenu");

        }

        if (device.GetPressUp(EVRButtonId.k_EButton_SteamVR_Touchpad) && !vectorsRecorded)
        {
            //LogDebug(forDebugging);
            vectorsRecorded = true;

        }

        if (device.GetPressUp(EVRButtonId.k_EButton_ApplicationMenu) && isMenuDisplayed)
        {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Button");
            foreach(GameObject button in buttons)
            {
                Destroy(button);
            }
            isMenuDisplayed = false;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        GameObject collidedObject = col.collider.gameObject;
        Rigidbody collidedRigidBody = collidedObject.GetComponent<Rigidbody>();

        if (collidedObject.tag == "Right" || collidedObject.tag == "Left")
        {
			
            return;
        }

        if (collidedRigidBody == null)
        {
            Debug.Log("Collision object has no rigidbody");
            Debug.Log(collidedObject.name);
        }

        if (collidedRigidBody != null)
        {
            Vector3 relativeVel = -(device.velocity + collidedRigidBody.velocity);

           

            forDebugging[0] = "Device velocity: " + device.velocity.ToString();
            forDebugging[1] = "Relative velocity: " + relativeVel.ToString();


            vectorsRecorded = false;

            float bonusPower = device.velocity.sqrMagnitude;


            collidedRigidBody.useGravity = true;

            Debug.Log("Collision object has no rigidbody");
            Debug.Log(collidedObject.name);

			// We can start the game with player touching gloves
        }

        gloveSoundManager.PlayPunchSound();

        Rumble();
    }

    public void Rumble()
	{
		device.TriggerHapticPulse (500);
	}
}
