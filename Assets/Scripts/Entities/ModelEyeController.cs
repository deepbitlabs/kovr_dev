﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MORPH3D;
using MORPH3D.FOUNDATIONS;

public class ModelEyeController : MonoBehaviour {

    M3DCharacterManager m3D;
    System.Random r;

    // For Eyes
    public int MinBlinkTime = 100;

    public int MaxBlinkTime = 400;

    public int MinBlinkRate = 2000;

    public int MaxBlinkRate = 10000;


    // For Face


    void Start()
    {
        m3D = GetComponent<M3DCharacterManager>();
        r = new System.Random();
        Invoke("BlinkTimer", r.Next(MinBlinkRate, MaxBlinkRate) / 1000f);
    }

    void BlinkTimer()
    {
        StartCoroutine("Blink");
        Invoke("BlinkTimer", r.Next(MinBlinkRate, MaxBlinkRate) / 1000f);
    }

    IEnumerator Blink()
    {
        m3D.SetBlendshapeValue("eCTRLEyesClosedR", 100);
        m3D.SetBlendshapeValue("eCTRLEyesClosedL", 100);
        yield return new WaitForSeconds(r.Next(MinBlinkTime, MaxBlinkTime) / 1000f);
        m3D.SetBlendshapeValue("eCTRLEyesClosedR", 0);
        m3D.SetBlendshapeValue("eCTRLEyesClosedL", 0);
    }
}
