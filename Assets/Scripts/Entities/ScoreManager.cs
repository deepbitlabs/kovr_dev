﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text score;
    public Text highScore;

    public float scoreValue;

    void Start()
    {
        highScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    void Awake()
    {
        score = GetComponent<Text>();

        scoreValue = 0f;

    }

    public void UpdateScores()
    {
        scoreValue++;
        score.text = scoreValue.ToString();

        PlayerPrefs.SetFloat("HighScore", scoreValue);
    }
}
