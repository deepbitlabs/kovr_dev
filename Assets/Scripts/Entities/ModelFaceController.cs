﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MORPH3D;
using MORPH3D.FOUNDATIONS;

public class ModelFaceController : MonoBehaviour
{
    M3DCharacterManager m3D;
    public float timer;
    private static float eyeValue = 100;
    float breathValue = 0;
    public float eyeSpeed;
    public float headBobValue;
    public float headBobSpeed;
    public float faceChangeValue;
    public float faceChangeSpeed;

    public float transformSpeed;
    public float transformValue;

    bool safeToStart;
    bool isAwake;
    bool isSleeping;
    bool breathOut;

    AudioSource audioSource;
    public AudioClip hurt;
    public AudioClip snore;

    float timePassed = 0;

    public bool warning = false;

    void Start()
    {
        m3D = GetComponent<M3DCharacterManager>();
        safeToStart = false;
        isAwake = false;
        isSleeping = false;
        breathOut = false;
        audioSource = GetComponent<AudioSource>();
        transformValue = 0;
    }

    void Update()
    {
        if (isAwake == true)
        {
            warning = false;
            isSleeping = false;
            eyeValue = eyeValue -= Time.deltaTime * eyeSpeed;
            m3D.SetBlendshapeValue("eCTRLEyesClosed", eyeValue);
            transformValue = transformValue += Time.deltaTime * transformSpeed;

            m3D.SetBlendshapeValue("eCTRLAngry", transformValue);
            m3D.SetBlendshapeValue("eCTRLScream", transformValue);
            m3D.SetBlendshapeValue("Eyelid_Size", transformValue);
            m3D.SetBlendshapeValue("EyesIrisSize", transformValue);
            m3D.SetBlendshapeValue("FaceCenterDepth", transformValue);

            
        }

        if (isSleeping == true)
        {
            isAwake = false;
            StartCoroutine(WaitToLight(1));
            transformValue = 0;
            eyeValue = 100;
            m3D.SetBlendshapeValue("eCTRLAngry", transformValue);
            m3D.SetBlendshapeValue("eCTRLScream", transformValue);
            m3D.SetBlendshapeValue("Eyelid_Size", transformValue);
            m3D.SetBlendshapeValue("EyesIrisSize", transformValue);
            m3D.SetBlendshapeValue("FaceCenterDepth", transformValue);
            m3D.SetBlendshapeValue("eCTRLEyesClosed", eyeValue);

            if (breathOut == false)
            {
                if (breathValue + Time.deltaTime < headBobValue)
                {
                    breathValue += Time.deltaTime * headBobSpeed;
                    faceChangeValue += Time.deltaTime + faceChangeSpeed;
                }
                else
                {
                    breathValue = headBobValue;
                    breathOut = true;
                }
            }
            else
            {
                if(breathValue - Time.deltaTime > 0)
                {
                    breathValue -= Time.deltaTime * headBobSpeed;
                    faceChangeValue -= Time.deltaTime * faceChangeSpeed;
                }
                else
                {
                    breathValue = 0;
                    breathOut = false;
                }
            }

            m3D.SetBlendshapeValue("Aged_Posture", breathValue);
            m3D.SetBlendshapeValue("eCTRLExcitement", faceChangeValue);
        }

        if (eyeValue <= 0)
        {
            eyeValue = 0;
        }

        if (transformValue >= 100)
        {
            transformValue = 100;
        }
    }

    IEnumerator WaitForStart(float time)
    {
        yield return new WaitForSeconds(time);
        safeToStart = true;
    }

    IEnumerator WaitToLight(float time)
    {
        yield return new WaitForSeconds(time);
    }
}
