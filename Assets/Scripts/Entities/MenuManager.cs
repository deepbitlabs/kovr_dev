﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour 
{
	private EnemyAI enemyAI;

	private EnemyController enemyController;

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Left" || col.gameObject.tag == "Right") 
		{

			// Main Menu Buttons
			if (gameObject.tag == "FightNow") 
			{
				SceneManager.LoadScene ("Stage1");
			}

			if (gameObject.tag == "ChallengeButton") 
			{
				SceneManager.LoadScene ("SelectChallenge");
			}

			// Challenge Menu Buttons
			if (gameObject.tag == "SpeedBagButton") 
			{
				//SceneManager.LoadScene ("SpeedBag");
				Debug.Log(col.gameObject.tag + " has selected " + gameObject.tag);
			}

			if (gameObject.tag == "HeavyBagButton") 
			{
				SceneManager.LoadScene ("PunchingBag");
				Debug.Log(col.gameObject.tag + " has selected " + gameObject.tag);
			}

			if (gameObject.tag == "ComboDummyButton") 
			{
				SceneManager.LoadScene ("ComboDummy");
				Debug.Log(col.gameObject.tag + " has selected " + gameObject.tag);
			}


			// Victory Button
			if (gameObject.tag == "Retry") 
			{
				Scene scene = SceneManager.GetActiveScene ();
				SceneManager.LoadScene (scene.buildIndex);
			}

			if (gameObject.tag == "ReturnToMenu") 
			{
				SceneManager.LoadScene ("MainMenu");
			}

			if (gameObject.tag == "ReturnToChallenge") 
			{
				SceneManager.LoadScene ("SelectChallenge");
			}

			// Session Buttons
			if (gameObject.tag == "ChangeChallenge") 
			{
				SceneManager.LoadScene ("SelectChallenge");
			}

			// Opponent select menu

			if (gameObject.tag == "Elnerdo") 
			{
				SceneManager.LoadScene ("Stage1");
			}

			if (gameObject.tag == "Bull") 
			{
				SceneManager.LoadScene ("Stage2");
			}

			if (gameObject.tag == "Jay") 
			{
				SceneManager.LoadScene ("Stage3");
			}

			if (gameObject.tag == "Sam") 
			{
				SceneManager.LoadScene ("Stage4");
			}
		}
	}
}
